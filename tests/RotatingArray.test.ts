import RotatingArray from '../src/RotatingArray';

describe('RotatingArray test suite', () => {
  let numberArray: RotatingArray<number>;

  beforeAll(() => {
    numberArray = new RotatingArray(5);
  });
  afterEach(() => {
    numberArray = new RotatingArray(5, [1, 2, 3, 4, 5]);
    //stringArray = new RotatingArray(5);
    //numberArrayWithData = new RotatingArray(5, [1, 2, 3]);
  });

  test('Create RotatingArray<string>', () => {
    const array = new RotatingArray<string>(20);
    expect(array.maxLength).toEqual(20);
  });

  test('Create RotatingArray<string> with prefilled data', () => {
    const array = new RotatingArray<string>(20, ["a", "b", "c"]);

    expect(array.length).toEqual(3);
  });

  test('Create RotatingArray<number> with too much initial data', () => {
    const array = new RotatingArray<number>(3, [1, 2, 3, 4, 5]);

    expect(array[0]).toEqual(3);
  })

  // [1, 2, 3, 4, 5]
  test('RotatingArray<number>#push', () => {
    numberArray.push(1);
    expect(numberArray.length).toEqual(5); // length should still be five
    expect(numberArray[0]).toEqual(2); // index 0 ("1") should have been dropped, which means index 0 should be 2.

    numberArray.push(3, 5, 7, 9); // should look like [1, 3, 5, 7, 9]
    expect(numberArray.length).toEqual(5); // length should still be five
    expect(numberArray[4]).toEqual(9); // indices 0-4 should have been dropped, which means index 0 should be 1.

    numberArray.push(6, 8, 10); // [1, 3, 5, 7, 9] => [1, 3, 5, 7, 9, 6, 8, 10] => [7, 9, 6, 8, 10]
    expect(numberArray.length).toEqual(5);
    expect(numberArray[0]).toEqual(7); // this one's the important one!
  });

  // [1, 2, 3, 4, 5]
  test('RotatingArray<number>#unshift', () => {
    numberArray.unshift(0); // should look like [0, 1, 2, 3, 4]
    expect(numberArray.length).toEqual(5);
    expect(numberArray[0]).toEqual(0);

    numberArray.unshift(-4, -3, -2, -1); // [0, 1, 2, 3, 4] => [-4, -3, -2, -1, 0, 1, 2, 3, 4] => [-4, -3, -2, -1, 0]
    expect(numberArray.length).toEqual(5);
    expect(numberArray[0]).toEqual(-4);
    expect(numberArray[4]).toEqual(0);
  });

  // [1, 2, 3, 4, 5]
  test('RotatingArray<number>#rotate', () => {
    numberArray.rotate(1); // rotate to the right once
    // [1, 2, 3, 4, 5] => [5, 1, 2, 3, 4]
    expect(numberArray[0]).toEqual(5);
    expect(numberArray.length).toEqual(5);

    numberArray.rotate(3); // rotate to the right three times
    // [5, 1, 2, 3, 4] => [2, 3, 4, 5, 1]
    expect(numberArray[0]).toEqual(2);
    expect(numberArray[3]).toEqual(5);
    expect(numberArray.length).toEqual(5);

    numberArray.rotate(3, true); // rotate to the left three times
    // [2, 3, 4, 5, 1] => [5, 1, 2, 3, 4]
    expect(numberArray[0]).toEqual(5);
    expect(numberArray[4]).toEqual(4);
  });

  test('RotatingArray<number>#shrink', () => {
    numberArray.pop();
    numberArray.pop();

    expect(numberArray.length).toEqual(3);
    expect(numberArray.maxLength).toEqual(5);

    numberArray.shrink();
    expect(numberArray.maxLength).toEqual(3);
  });
});
