# RotatingArray

Simple class that provides an array-like structure. Clamps length of the array to a maximum length, and upon `push` or `unshift`, it removes the necessary elements on the respective side to keep it at the maximum length.

## Getting started
* Install the `rotating-array` package with your favorite package manager (mine is [pnpm](https://pnpm.io/) <3)
* **This package is ESM only!** You cannot `require("rotating-array")`!

```ts
import RotatingArray from "rotating-array";

// Initialize a new RotatingArray instance with a maximum array length of five.
// If you are using this in TypeScript, you can also set the type of the array.
const array = new RotatingArray<number>(5);

// You may also seed the array upon initialization: 
new RotatingArray<number>(5, [1, 2, 3, 4, 5]);
```

## The differences

`RotatingArray#push` and `RotatingArray#unshift` behave a bit differently. This is why this package was created.

```ts
// Using our array from above, it has an array of [1, 2, 3, 4, 5].
// If I push a new element onto the array:

array.push(6);

// the new array will *not* be [1, 2, 3, 4, 5, 6] but will instead be
// [2, 3, 4, 5, 6] because of the maximum size being clamped to 5.

// Similarly,
array.push(7, 8, 9, 10);
// [2, 3, 4, 5, 6] => [6, 7, 8, 9, 10]
```

Unshift works exactly the same but in the opposite direction. Elements with higher indices will be removed to make room for the new elements.

## The additions
* `RotatingArray#rotate`\
Rotates the array `n` steps to the right (by default). You may also specify `rotate(n, true)` to rotate it to the left.\
For example:
```ts
const array = new RotatingArray<number>(5, [1, 2, 3, 4, 5]);

array.rotate(3); // [1, 2, 3, 4, 5] => [3, 4, 5, 1, 2]
array.rotate(8); // [3, 4, 5, 1, 2] => [5, 1, 2, 3, 4]

array.rotate(1, true); // [5, 1, 2, 3, 4] => [1, 2, 3, 4, 5]
```

* `RotatingArray#shrink`\
Shrinks the maximum length down to the current length of the array.\
For example:
```ts
const array = new RotatingArray<number>(5, [1, 2, 3, 4, 5]);

array.pop(); // [1, 2, 3, 4]
array.pop(); // [1, 2, 3]

array.shrink(); // maximum length is now 3
// Do keep in mind that this process is **IRREVERSIBLE**

array.push(4); // [2, 3, 4]
```

## Author's note
At the time of writing, I wouldn't consider this package to be stable *just yet*, but as always semver will tell you if things have changed. During the `0.x.x` cycle, I will try and adhere as close as I can to the semver standard, but the second digit (not the first) will become the "breaking change" digit.

If you would like to contribute, feel free to open an issue or a PR at the gitlab repo: https://gitlab.com/akii0008/rotatingarray. This is my first ever published package, so any feedback would be greatly appreciated! <3

License: [MIT](LICENSE.md)\
Changelog: [CHANGELOG.md](CHANGELOG.md)