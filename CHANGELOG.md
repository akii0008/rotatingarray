# Changelog

## 0.0.4
* Pass the generic type along to the Array class we extend

## 0.0.3
* **The `RotatingArray` class now extends the native Array class for better compatibility** (and also so I don't have to completely re-implement every array method lmao)
* Removed local `pop` method
* Removed local `shift` method
* Removed local `at` method
* Removed local `array` getter
* Removed local `length` getter
* Updated tests accordingly.
  * Also, before each test, the array is re-created with a max length of 5 and initial data of `[1, 2, 3, 4, 5]`

## 0.0.2
* Added `RotatingArray#rotate` method: Rotates array n elements to the right, or n to the left if boolean "reverse" is passed set to true.

## 0.0.1
* Created `RotatingArray` typed class
* Created `push` method: Pushes an element to the end of the array, removing elements at the beginning if necessary
* Created `pop` method: Removes the last element from the array and returns it
* Created `shift` method: Removes the first element from the array and returns it
* Created `unshift` method: Adds an element to the beginning of the array, removing elements at the end if necessary
* Created `at` method: Gets the array at the specified index
* Created `shrink` method: Changes the maximum length to the current size of the array
* Created `array` getter: Returns the internal array
* Created `length` getter: Returns the length of the internal array
* Created `maxLength` getter: Returns the array's maximum length
* Uses [jest](https://jestjs.io/) testing framework