export default class RotatingArray<T> extends Array<T> {

  private maximumLength: number;

  /**
   * Creates a new RotatingArray
   * @param maximumLength Maximum length of the RotatingArray
   * @param initialData Initial data to seed the RotatingArray
   */
  constructor(maximumLength: number, initialData?: T[]) {
    super();

    if (maximumLength === 0)
      console.warn('Warning: Initializing RotatingArray with maximum length of zero.');
    this.maximumLength = maximumLength;

    if (initialData) {
      this.push(...initialData);
    }
  }

  /**
   * Push new items onto the array. Will remove array's first-most elements upon maximumLength being reached.
   * @param elements Elements to push onto the array.
   */
  public override push(...elements: T[]): number {
    super.push(...elements);

    if (this.length > this.maximumLength) {
      const elementsTooMany = this.length - this.maximumLength;
      for (let i = 0; i < elementsTooMany; i++) {
        this.shift();
      }
    }

    return this.length;
  }

  /**
   * Unshift new elements onto the array. Will remove array's last-most elements upon maximumLength being reached.
   * @param elements Elements to unshift onto the array. 
   */
  public override unshift(...elements: T[]): number {
    super.unshift(...elements);

    if (this.length > this.maximumLength) {
      const elementsTooMany = this.length - this.maximumLength;
      for (let i = 0; i < elementsTooMany; i++) {
        this.pop();
      }
    }

    return this.length;
  }

  /**
   * Rotates the array to the right, and if reverse is set, to the left.
   * @param numRotations Number of rotations
   * @param reverse Reverse the rotation (to the left)
   * @returns The new, rotated array
   */
  public rotate(numRotations: number, reverse?: boolean): T[] {
    if (this.length === 0) {
      throw new RangeError('Cannot rotate empty array');
    }

    for (let i = 0; i < numRotations; i++) {
      reverse
        ? this.push(this.shift()!)
        : this.unshift(this.pop()!);
    }

    return this;
  }

  /**
   * Shrinks the array's maximum length down to the current array length.
   * <b>This process is IRREVERSIBLE</b>
   * @returns {number} The new maximum length
   */
  public shrink(): number {
    this.maximumLength = this.length;

    return this.maximumLength;
  }

  /**
   * Gets the maximum length of this RotatingArray instance
   */
  public get maxLength() { return this.maximumLength; }

}